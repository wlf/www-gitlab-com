---
layout: handbook-page-toc
title: "DevOps Platform GTM Sales Plays"
description: "List and description of all Sales Plays in the DevOps Platform GTM motion"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## DevOps Platform Motion 

### Integrated campaigns
1. TBD
   - Campaign brief (placeholder)

### Sales plays

1. [Sales Playbook for DevOps Platform](/handbook/marketing/plan-fy22/gtm-devops-platform/devops-platform-planner/devops-platform-playbook)
   - [Message house](/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/message-house/)
   - [Strategy/planner doc](/handbook/marketing/plan-fy22/gtm-devops-platform/devops-platform-planner) for visibility

Additional sales plays will be added as they become available.

#### Other resources

