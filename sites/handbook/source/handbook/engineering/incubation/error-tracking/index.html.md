---
layout: handbook-page-toc
title: Error Tracking Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Error Tracking Single-Engineer Group

The Error Tracking SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation). 

The goal of the Error Tracking SEG is to provide monitoring tools for that allow developers to identify and resolve issues in their production environments.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329596]()
