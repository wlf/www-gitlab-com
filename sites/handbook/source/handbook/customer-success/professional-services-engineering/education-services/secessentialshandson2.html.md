---
layout: handbook-page-toc
title: "GitLab Security Essentials Hands On Guide- Lab 2"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab Security Essentials course."
---
# GitLab Security Essentials Hands On Guide- Lab 2
{:.no_toc}

## LAB 2- Review and create actions on Vulnerabilities

### View the scanning results in the Security Dashboard 
1. Validate your pipeline has completed and passed
2. Navigate to **Security & Compliance** in the left menu pane  
3. Click on the **Vulnerability Report** tab -> Click a critical vulnerability in the report field.
4. Review the vulnerability information and click the **status** button. In the dropdown choose **Dismiss**
5. Click on the **Vulnerability Report** tab -> Click a critical vulnerability in the report field.
6. Review the vulnerability information and click the **status** button. In the dropdown choose **Confirm**
7. On the critical vulnerability report, click **create issue** button.  Assign the ticket to yourself and **submit issue**.
8. Review the Issue and the solution.  Review the link back to the vulnerability report.
9. On the left-hand panel, click the **Security and Compliance** section and Vulnerability Report tab. -> Click a critical vulnerability in the report field.
10. Review the vulnerability information and click the **status** button. In the dropdown choose **Resolved**
11. Review the different information displayed, the status, severity, and scanner types shown in the Dashboard. Set the **status** dropdown to **All**
12. Once you have reviewed all of the information, click on the other reports that appears.
13. Click the **Configure** button in the Manage column to view the specific configuration options available for your SAST Scanner. Once you have reviewed the options available, you can close out of this section.

### Read an Artifacts for the Scanning Reports
1. Click on the **CI/CD** button on the Left menu.  
2. On the pipeline click the artifacts dropdown menu.  
3. Select various reports and review 


### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab Security Essentials- please submit your changes via Merge Request!

